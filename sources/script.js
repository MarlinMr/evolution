var spritesheet 		= new Image();
    spritesheet.src 	= "./sources/spritesheet.png";
var worldWidth = 1300;
var random = [];
var worldHeight = 700;
var tick = 0;
var dots=[];
var script = {fps:60};
var maxSteps, maxGraph = 250;
var population = 100;
var moveSpeed = 25;
var gen = 0;
var ary = {x:1,y:2};
var newSpecies = 0.1;
var ary2 = {};
var fittest = 0;
var numberSpecies = 10;
var fittestIndex = 0;
var sumFitness = 0;
var species = ["AP","FrP","H","KrF","MdG","PP","R","Sp","SV","V"];
var breedRate = 2;
var mutationRate = 0.005;
var fittestDot = {};
var spawn = {x:worldWidth-20, y:worldHeight-20};
var goal = {x:20,y:20,radius:4,fillStyle:'#0000ff'}
var fitnessHistory = [];
var fitnessHistoryFittness = [];

function loadVariables(){
    maxSteps        = Number(inputMaxSteps.value);
    moveSpeed       = Number(inputMoveSpeed.value);
    population      = Number(inputPopulation.value);
    mutationRate    = Number(inputMutationRate.value);
    goal.x          = Number(inputGoalX.value);
    goal.y          = Number(inputGoalY.value);
    spawn.x         = Number(inputSpawnX.value);
    spawn.y         = Number(inputSpawnY.value);
    script.fps      = Number(inputScriptFPS.value);
    breedRate       = Number(inputBreedRate.value);
    newSpecies      = Number(inputNewSpecies.value);
}

function playGeneration(){
    tick=0;
    script.inerval = setInterval(loop,1000/script.fps);
}

function loop(){
    if(tick<maxSteps){
        moveDots(tick);
        if(document.getElementById("animate").checked){
            drawDots();
        } 
        tick++;
    }else{
        clearInterval(script.inerval);
        calculateFitness();
        gen++;
        console.log("Gen:",gen,"Fittest:",fittestIndex,"Fitness:",fittest,"Steps:",dots[fittestIndex].steps,"totalFitness:",sumFitness/dots.length);
        if(document.getElementById("autorun").checked){
            clone();
        }
    }
}

function clone(){
    var newDots=[];
    for(var c=0;c<dots.length;c++){
        newDots[c]={...selectParent()};
        newDots[c].x=spawn.x
        newDots[c].y=spawn.y
        newDots[c].dead=false;
        newDots[c].reachedGoal=false;
        newDots[c].steps=0;
    }
    dots=[...newDots];
    mutate();
    dots[0]={...fittestDot};
    dots[0].x=spawn.x
    dots[0].y=spawn.y
    dots[0].dead=false;
    dots[0].reachedGoal=false;
    dots[0].steps=0;
    if(document.getElementById("autorun").checked){
        playGeneration();
    }
}

function mutate(){
    for(var k=0;k<dots.length;k++){
        var mutations = 0;
        for(var l=0;l<dots[k].pathX.length;l++){
            var rand=Math.random();
            if(rand<mutationRate){
                dots[k].pathX[l]=(Math.random()*2-1)*Math.random()*moveSpeed;
                dots[k].pathY[l]=(Math.random()*2-1)*Math.random()*moveSpeed;
                mutations++;
            }
        }
        //console.log("Mutations:",mutations);
        if(rand<(mutationRate*newSpecies)){
            dots[k].sprite=Math.floor(Math.random()*numberSpecies);
            console.log("New Species!",species[dots[k].sprite]);
            var number = '#'+Math.floor(Math.random()*16777215).toString(16);
            dots[k].fillStyle=number;
        }   
    }
}
    
function selectParent(){
    var rand = Math.random()*sumFitness;
    var runningSum = 0;
    for(var j=0;j<dots.length;j++){
        runningSum+=dots[j].fitness;
        if(runningSum>=rand){
            return {dead:false, 
                    sprite:dots[j].sprite, 
                    x: spawn.x, y: spawn.y, 
                    radius: 2, steps:0, 
                    pathX:[...dots[j].pathX],pathY:[...dots[j].pathY],
                    fillStyle:dots[j].fillStyle,fitness:0};
        }
    }
} 

function calculateFitness(){
    fittest=0;
    sumFitness=0;
    for(var i = 0;i<dots.length;i++){
        distanceToGoal=calcDistance(dots[i]);
        if(document.getElementById("evenFit").checked){
            dots[i].fitness=(1/(dots[i].steps +distanceToGoal));
        }else{
            if(dots[i].reachedGoal){
                 dots[i].fitness=(1/Math.pow(distanceToGoal,breedRate))+(1/dots[i].steps);
            }else{  
                dots[i].fitness=1/Math.pow(distanceToGoal,breedRate);
            }
        }
        if(dots[i].fitness>fittest){
            fittest=dots[i].fitness;
            fittestIndex=i;
        }
        if(dots[fittestIndex].reachedGoal){
            maxSteps=dots[fittestIndex].steps;
        }
    }
    for(var i=0;i<dots.length;i++){
        sumFitness+=dots[i].fitness;
    }
    //dots[fittestIndex].fillStyle='#ff00ff';
    drawDot(dots[fittestIndex],'#ff00ff');
    fittestDot = {...dots[fittestIndex]};
	fitnessHistory.push(dots[fittestIndex].steps);
	fitnessHistoryFittness.push(dots[fittestIndex].fitness);
	drawGraph();
	
}

function calcDistance(dot){
    fitX=Math.abs(dot.x-goal.x);
    fitY=Math.abs(dot.y-goal.y);
    return Math.sqrt((fitX*fitX)+(fitY*fitY));
}

function drawDot(dot,fillStyle) {
  ctx.beginPath();
  ctx.arc(dot.x, dot.y, dot.radius, 0, 2 * Math.PI, false);
  ctx.fillStyle = fillStyle;
  ctx.fill();
}

function generateGeneration(){
    for(var i=0; i<population; i++){
        var number = '#'+Math.floor(Math.random()*16777215).toString(16);
        dots[i]={dead:false, 
                sprite:Math.floor(Math.random()*numberSpecies), 
                reachedGoal:false, 
                x: spawn.x, y: spawn.y, 
                radius: 2, steps:0, 
                pathX:randomSequence(),pathY:randomSequence(),
                fillStyle:number,fitness:0};
    }
    drawDots();
}

function drawDots(){
    ctx.clearRect(0, 0, canvas.width, canvas.height);
    var circle=[{x:goal.x,y:goal.y,radius:100,fillStyle: '#c0c0a0'},
				{x:goal.x,y:goal.y,radius:200,fillStyle: '#b5b5a0'},
				{x:goal.x,y:goal.y,radius:300,fillStyle: '#b0b0a0'},
				{x:goal.x,y:goal.y,radius:400,fillStyle: '#a5a5a0'},
				{x:goal.x,y:goal.y,radius:500,fillStyle: '#a0a0a0'},
				{x:goal.x,y:goal.y,radius:600,fillStyle: '#9595a0'},
				{x:goal.x,y:goal.y,radius:700,fillStyle: '#9090a0'},
				{x:goal.x,y:goal.y,radius:800,fillStyle: '#8585a0'},
				{x:goal.x,y:goal.y,radius:900,fillStyle: '#8080a0'},
				{x:goal.x,y:goal.y,radius:1000,fillStyle:'#7575a0'},
				{x:goal.x,y:goal.y,radius:1100,fillStyle:'#7070a0'},
				{x:goal.x,y:goal.y,radius:1200,fillStyle:'#6565a0'},
				{x:goal.x,y:goal.y,radius:1300,fillStyle:'#6060a0'},
				{x:goal.x,y:goal.y,radius:1400,fillStyle:'#5555a0'},
				{x:goal.x,y:goal.y,radius:1500,fillStyle:'#5050a0'},
				{x:goal.x,y:goal.y,radius:1600,fillStyle:'#4545a0'}];
    for(var i=circle.length-1;i>=0;i--){
        drawDot(circle[i],circle[i].fillStyle);
    }
    for(var i=0;i<dots.length;i++){
        if(!document.getElementById("sprite").checked){drawDot(dots[i],dots[i].fillStyle);}
        if(document.getElementById("sprite").checked){drawSprite(dots[i]);}
    }
    drawDot(goal,goal.fillStyle);
}

function moveDots(j){
    for(var i=0;i<dots.length;i++){
        if(!dots[i].dead && !dots[i].reachedGoal){
            dots[i].x+=dots[i].pathX[j];
            dots[i].y+=dots[i].pathY[j];
            dots[i].steps++;
            if(dots[i].x<2 || dots[i].y<2 || dots[i].x>worldWidth-2 || dots[i].y>worldHeight-2){
                dots[i].dead=true;
            }else if(dots[i].x<(goal.x +goal.radius) && dots[i].y<(goal.y +goal.radius) && dots[i].x>(goal.x -goal.radius) && dots[i].y>(goal.y -goal.radius)){
                dots[i].reachedGoal=true;
            }
        }
    }
}

function randomSequence(){
    var list = [];
    for(var i = 0;i<maxSteps;i++){
        list[i]=(Math.random()*2-1)*Math.random()*moveSpeed;
    }
    return list;
}

function drawSprite(dot){ctx.drawImage(spritesheet,dot.sprite*30,0,32,32,dot.x-16,dot.y-16,32,32);}

function onLoad(){
	canvas = document.getElementById("GUICanvas");
	canvas.width = worldWidth;
	canvas.height = worldHeight;
	ctx = canvas.getContext("2d");	
	graphCanvas = document.getElementById("Graph");
	graphCanvas.width = 500;
	graphCanvas.height = maxGraph;
	gctx = graphCanvas.getContext("2d");
}

function drawGraph(){
	gctx.clearRect(0, 0, graphCanvas.width, graphCanvas.height);
	gctx.beginPath();
	gctx.moveTo(0,0);
	gctx.strokeStyle = "#00FF0";
	for (i=0;i<fitnessHistory.length;i++){
		gctx.lineTo((graphCanvas.width/fitnessHistory.length)*(i+1),((maxGraph-fitnessHistory[i])*(maxGraph/graphCanvas.height)));
		gctx.stroke();
		gctx.moveTo((graphCanvas.width/fitnessHistory.length)*(i+1),((maxGraph-fitnessHistory[i])*(maxGraph/graphCanvas.height)));
	}
	gctx.moveTo(0,maxGraph);
	gctx.strokeStyle = "#FF0000";
	for (i=0;i<fitnessHistoryFittness.length;i++){
		gctx.lineTo((graphCanvas.width/fitnessHistoryFittness.length)*(i+1),(graphCanvas.height-graphCanvas.height*(fitnessHistoryFittness[i]/fitnessHistoryFittness[fitnessHistoryFittness.length-1])));
		gctx.stroke();
		gctx.moveTo((graphCanvas.width/fitnessHistoryFittness.length)*(i+1),(graphCanvas.height-graphCanvas.height*(fitnessHistoryFittness[i]/fitnessHistoryFittness[fitnessHistoryFittness.length-1])));
	}
} 

